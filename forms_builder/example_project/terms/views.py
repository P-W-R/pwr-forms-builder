import hashlib
import json
import requests
from contextlib import suppress
from requests.exceptions import ConnectionError

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import (
    ValidationError,
    MultipleObjectsReturned,
)
from django.core.validators import validate_email
from django.utils.translation import ugettext_lazy as _

from phonenumber_field.validators import validate_international_phonenumber
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from simplejson.scanner import JSONDecodeError as JSONDecodeErrorSimplejson

from utils import get_client_ip


def check_connect():
    """Checks API connection, return False if there is no connection."""

    url = settings.AGREEMENTS_API_URL
    command = 'check_connect'

    try:
        return requests.post(url, data={'command': command}).json()
    except (json.JSONDecodeError, JSONDecodeErrorSimplejson, ConnectionError):
        return False


def get_perms_type(description=None):
    """Returns list of permissions."""

    url = settings.AGREEMENTS_API_URL
    command = 'get_perms_type_full'
    try:
        perms_types = requests.post(
            url,
            data={
                'command': command,
                'description': description,
            }
        ).json().get('perms')
    except (
        json.JSONDecodeError,
        JSONDecodeErrorSimplejson,
        ConnectionError
    ):
        perms_types = None

    return perms_types


def find_email(data):
    """ Finds and returns a first email value found in f.e. POST data.

        :param QueryDict data: Data in which to look for email field.
    """

    for field in data.values():
        try:
            validate_email(field)
        except ValidationError:
            pass
        else:
            return field
    return None


def find_phone_number(data):
    """ Finds and returns a first phone number value found in f.e. POST data.

        :param QueryDict data: Data in which to look for phone number field.
    """

    for field in data.values():
        try:
            validate_international_phonenumber(field)
        except ValidationError:
            pass
        else:
            return field
    return None


class UpdateUserView(APIView):
    allowed_methods = ['post']
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        from terms.serializers import (
            TermsUserProfileSerializer,
            TermsSerializer,
        )

        url = settings.AGREEMENTS_API_URL
        command = 'update_user'

        # Check if user have to be logged in
        # 1. logged in user - get user from request
        # 2. anonymous user - try to get user from given email

        user_required = kwargs.get('user_required')
        user = None
        if user_required and request.user.is_authenticated():
            user = request.user
        else:
            with suppress(User.DoesNotExist, MultipleObjectsReturned):
                user = User.objects.get(
                    email__iexact=request.data.get('email')
                )

        terms_serializer = TermsSerializer(
            data=request.data,
            context={
                'command': command,
                'request': request,
            }
        )

        if terms_serializer.is_valid():
            data = terms_serializer.data
            email = data.get('email')
            phone = data.get('phone')

            # Add user's profile's data to data dict if user was found
            if user:
                if not email:
                    email = user.email
                    data['email'] = email
                if not phone:
                    phone = str(user.profile.phone)
                    data['phone'] = phone
                if user.email == email:
                    serializer = TermsUserProfileSerializer(
                        user.profile
                    )
                    data.update(serializer.data)

            try:
                requests.post(
                    url,
                    data=data,
                )
                return Response(status=status.HTTP_200_OK)
            except (
                json.JSONDecodeError,
                JSONDecodeErrorSimplejson,
                ConnectionError,
            ) as e:
                return Response(
                    data={'exceptions': e},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                data={'errors': terms_serializer.errors},
                status=status.HTTP_400_BAD_REQUEST
            )


def update_user(request):
    """Updates user permissions. Requires 'mail' variable to be in request."""

    url = settings.AGREEMENTS_API_URL
    command = 'update_user'
    mail = find_email(request.POST)
    phone = find_phone_number(request.POST)

    if not mail:
        try:
            mail = request.user.email
        except AttributeError:
            mail = None

    if not phone:
        try:
            phone = str(request.user.profile.phone)
        except AttributeError:
            phone = None

    data = {
        'command': command,
        'mail': mail,
        'phone': phone,
        'description': request.POST.get('description'),
        'marketing': request.POST.get('marketing'),
        'personal': request.POST.get('personal'),
        'tele': request.POST.get('tele'),
        'ip': get_client_ip(request),
    }

    from terms.serializers import TermsUserProfileSerializer

    if request.user.is_authenticated() and request.user.profile and \
            request.user.email == mail:
        serializer = TermsUserProfileSerializer(request.user.profile)
        data.update(serializer.data)
    else:
        with suppress(User.DoesNotExist):
            profile = User.objects.get(
                email=mail
            ).profile
            serializer = TermsUserProfileSerializer(profile)
            data.update(serializer.data)

    try:
        requests.post(
            url,
            data=data,
        )
    except (json.JSONDecodeError, JSONDecodeErrorSimplejson, ConnectionError):
        return None


def get_user_perms_history(user):
    """Returns user actual permissions, if any."""

    url = settings.AGREEMENTS_API_URL
    mail = hashlib.md5(user.email.encode('utf-8')).hexdigest()
    command = 'get_user_perms_history'

    try:
        json_text = requests.post(
            url,
            data={
                'command': command,
                'mail': mail
            }
        ).json().get('list')
        return json.loads(json_text)
    except (json.JSONDecodeError, JSONDecodeErrorSimplejson, ConnectionError):
        return None
