from django.conf import settings
from django.contrib.admin.widgets import AdminTextareaWidget

from tinymce.widgets import TinyMCE as OriginalTinyMCE


class TinyMCE(OriginalTinyMCE):
    @property
    def media(self):
        media = super(TinyMCE, self).media
        media.add_css({
            'all': [
                settings.STATIC_URL + 'tinymce/fonts.css',
            ]
        })
        return media


class AdminTinyMCE(AdminTextareaWidget, TinyMCE):
    pass
