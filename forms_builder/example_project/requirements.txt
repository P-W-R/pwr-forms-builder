Django==1.11.13
sorl-thumbnail==12.4.1
future==0.16.0
unidecode==0.04.16
requests==2.4.1
django-phonenumber-field==0.6
simplejson==3.10.0
django-multiselectfield==0.1.5
git+https://github.com/PiotrRybarczyk/django-tinymce.git#egg=django-tinymce
sphinx-me == 0.3
git+https://github.com/stephenmcd/django-email-extras.git@cf59a8ba583d5e6a6e7a3a9bd60a7ddec3a03320#egg=django-email-extras
Pillow>=2.4.0
djangorestframework==3.6.2
django-phonenumber-field==0.6