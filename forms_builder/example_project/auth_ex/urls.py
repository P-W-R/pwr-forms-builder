from django.conf import settings
from django.conf.urls import url
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.views import logout, password_reset_done, password_reset_confirm, password_reset_complete

from social_core.utils import setting_name
from subscriptions.views import user_subscriptions
from releases.views import user_releases, user_pdf_releases
from .views import login, profile, profile_edit, profile_shopping_history, profile_password_reset, password_reset, api_create_user, custom_fb_auth


extra = getattr(settings, setting_name('TRAILING_SLASH'), True) and '/' or ''


urlpatterns = [
    url(_(r'^login/$'), login, name='login'),
    url(_(r'^profile/$'), profile, name='profile'),
    url(_(r'^profile/edit/$'), profile_edit, name='profile_edit'),
    url(_(r'^profile/shopping-history/$'), profile_shopping_history, name='profile_shopping_history'),
    url(_(r'^profile/password-reset/$'), profile_password_reset, name='profile_password_reset'),
    url(_(r'^password-reset/$'), password_reset,
        {'post_reset_redirect': reverse_lazy('auth:password_reset_done')}, name='password_reset'),
    url('^api/create_user$', api_create_user, name='api_create_user'),
    url(r'^login/(?P<backend>[^/]+){0}$'.format(extra), custom_fb_auth, name='begin'),
]

urlpatterns += [
    url(_(r'^profile/subscriptions/$'), user_subscriptions, name='user_subscriptions'),
]

urlpatterns += [
    url(_(r'^profile/releases/$'), user_releases, name='user_releases'),
    url(_(r'^profile/pdf-releases/$'), user_pdf_releases, name='user_pdf_releases'),
]

urlpatterns += [
    url(_(r'^logout/$'), logout, {'next_page': reverse_lazy('homepage:index')}, name='logout'),
    url(_(r'^password-reset/done/$'), password_reset_done, name='password_reset_done'),
    url(_(r'^password-reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$'), password_reset_confirm,
        {'post_reset_redirect': reverse_lazy('auth:password_reset_complete')}, name='password_reset_confirm'),
    url(_(r'^password-reset/complete/$'), password_reset_complete, name='password_reset_complete'),
]
