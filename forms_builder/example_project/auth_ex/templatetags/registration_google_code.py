from django import template
from django.conf import settings


register = template.Library()


@register.inclusion_tag("registration/registration_complete_google.html", takes_context=True)
def registration_google_code(context):
    request = context.get('request')
    if request and request.session.pop('registration_google_code', False):
        try:
            return {
                'show_registration_google_code': True,
                'google_conversion_label': settings.GOOGLE_CONVERSION_LABEL,
            }
        except AttributeError:
            pass
    return {'show_registration_google_code': False}
