{% load i18n %}
{% trans "New user has registered." %}
{% trans "ID:" %} {{ user.pk }}
{% trans "Username:" %} {{ user.username }}
{% trans "Email:" %} {{ user.email }}
