{% load i18n l10n %}
{% trans "User has updated their profile." %}
{% trans "User ID:" %} {{ user.pk|unlocalize }}
{% trans "Username:" %} {{ user.username }}
{% trans "Email:" %} {{ user.email }}

{% trans "Basic data:" %}
{% trans "Account type:" %} {{ profile.get_type_display }}
{% trans "First name:" %} {{ profile.first_name }}
{% trans "Last name:" %} {{ profile.last_name }}
{% if profile.business %}{% trans "Company:" %} {{ profile.business }}
{% trans "NIP:" %} {{ profile.nip }}{% endif %}
{% trans "City:" %} {{ profile.city }}
{% trans "Street:" %} {{ profile.street }}
{% trans "Local number:" %} {{ profile.local_number }}
{% trans "ZIP Code:" %} {{ profile.zip_code }}
{% if profile.birthday %}{% trans "Birthday:" %} {{ profile.birthday }}{% endif %}
{% trans "Phone:" %} {{ profile.phone }}

{% trans "Correspondence address:" %}
{% if not profile.different_c_address %}{% trans "Same as in basic data" %}{% else %}
{% trans "City:" %} {{ profile.c_city }}
{% trans "Street:" %} {{ profile.c_street }}
{% trans "Local number:" %} {{ profile.c_local_number }}
{% trans "ZIP Code:" %} {{ profile.c_zip_code }}
{% trans "Phone:" %} {{ profile.c_phone }}{% endif %}

{% if profile.customer_id %}{% trans "Customer ID:" %} {{ profile.customer_id }}
{% trans "Card ID:" %} {{ profile.card_id }}{% endif %}

{% with ugcs=profile.user.good_quantities.all %}{% if ugcs %}
{% trans "Additional data" %}:
{% for ugc in ugcs %}
    {{ ugc.good }} - {{ ugc.quantity }}
{% endfor %}{% endif %}{% endwith %}
