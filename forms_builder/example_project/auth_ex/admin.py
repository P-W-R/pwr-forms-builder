from django import forms
from django.contrib import admin
from django.contrib.auth.admin import (
    UserAdmin as OriginalUserAdmin,
    UserCreationForm as OriginalUserCreationForm,
    UserChangeForm as OriginalUserChangeForm,
)
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from terms.views import get_user_perms_history

from .models import UserProfile, UserSite


class UserCreationForm(OriginalUserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email',)

    def clean_email(self):
        if User.objects.filter(email__iexact=self.cleaned_data.get('email')):
            raise forms.ValidationError(_('This email address is already in use. Please supply a different email address.'))
        return self.cleaned_data.get('email')


class UserChangeForm(OriginalUserChangeForm):
    username = forms.CharField(
        label=_('Username'),
        max_length=30,
        help_text=_('Required. 30 characters or fewer.'),
    )
    terms = forms.CharField(
        label=_('Terms and agreements'),
        widget=forms.Textarea,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        # set user terms field from API
        super(UserChangeForm, self).__init__(*args, **kwargs)
        terms = get_user_perms_history(self.instance)
        kwargs.update(initial={'terms': terms})
        super(UserChangeForm, self).__init__(*args, **kwargs)


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = _('Profile')
    exclude = ('activation_code_attempts', 'activation_code_ban')
    readonly_fields = ('logged_sites',)
    suit_classes = 'suit-tab suit-tab-general'


class UserSiteInline(admin.TabularInline):
    model = UserSite
    extra = 1
    verbose_name_plural = _('Sites')
    suit_classes = 'suit-tab suit-tab-general'


class UserForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        permissions = self.fields['permissions']
        permissions.queryset


class UserAdmin(OriginalUserAdmin):
    list_display = (
        'id',
        'username',
        'email',
        'profile_first_name',
        'profile_last_name',
        'profile_customer_id',
        'profile_card_id',
        'is_staff',
        'is_active',
    )
    list_display_links = (
        'id',
        'username',
    )
    inlines = (UserSiteInline, UserProfileInline)
    form = UserChangeForm
    add_form = UserCreationForm
    list_filter = (
        'is_staff',
        'is_superuser',
        'is_active',
        'sites__site',
        'groups',
    )
    search_fields = (
        'id',
        'username',
        'first_name',
        'last_name',
        'email',
        'profile__customer_id',
        'profile__card_id',
    )
    change_form_template = 'admin/change_form_terms.html'

    suit_form_tabs = (
        ('general', _('General')),
        ('terms', _('Terms'))
    )

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('username', 'email', 'password')}),
        (_('Permissions'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
                'groups',
                'user_permissions',
            )}),
        (_('Important dates'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('last_login', 'date_joined')}),
    )
    staff_fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('username', 'email', 'password',)}),
        (_('Important dates'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide', 'suit-tab', 'suit-tab-general'),
            'fields': ('username', 'email', 'password1', 'password2')
        }),
    )

    def change_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            # Disable permissions for non-superuser.
            try:
                self.fieldsets = self.staff_fieldsets
                response = super(UserAdmin, self).change_view(request, *args, **kwargs)
            finally:
                self.fieldsets = UserAdmin.fieldsets
            return response
        else:
            return super(UserAdmin, self).change_view(request, *args, **kwargs)


    def get_inline_instances(self, request, obj=None):
        if obj is None:
            return []
        return super(UserAdmin, self).get_inline_instances(request, obj)

    def get_urls(self):
        urls = super(UserAdmin, self).get_urls()
        urls[0].name = 'auth_user_change_password'
        return urls

    def profile_customer_id(self, obj):
        try:
            return obj.profile.customer_id
        except:
            pass
    profile_customer_id.short_description = _('Customer ID')
    profile_customer_id.admin_order_field = 'profile__customer_id'

    def profile_card_id(self, obj):
        try:
            return obj.profile.card_id
        except:
            pass
    profile_card_id.short_description = _('Card ID')
    profile_card_id.admin_order_field = 'profile__card_id'

    def profile_first_name(self, obj):
        try:
            return obj.profile.first_name
        except:
            pass
    profile_first_name.short_description = _('First name')
    profile_first_name.admin_order_field = 'profile__first_name'

    def profile_last_name(self, obj):
        try:
            return obj.profile.last_name
        except:
            pass
    profile_last_name.short_description = _('Last name')
    profile_last_name.admin_order_field = 'profile__last_name'


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
