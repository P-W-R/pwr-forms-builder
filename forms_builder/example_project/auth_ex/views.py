from functools import partial
import re

from django.conf import settings
from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import (
    PasswordResetForm
)
from django.contrib.auth.models import User
from django.contrib.auth.views import login as django_login, \
    password_reset as django_password_reset
from django.contrib.sites.models import Site
from django.urls import reverse, reverse_lazy
from django.db import DatabaseError
from django.db.transaction import atomic
from django.http.response import HttpResponseRedirect, HttpResponseForbidden, \
    HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.http import is_safe_url, urlencode
from django.utils.translation import ugettext as _
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST

from getpaid.models import Payment
from registration import signals
from registration.backends.default.views import \
    ActivationView as OriginalActivationView
from registration.models import RegistrationProfile
from registration.views import RegistrationView as BaseRegistrationView

from galleries.models import UserGallery
from goods.forms import GoodsFormSet, UserGoodCategoryFormSet
from releases.models import Release, UserPDFRelease, UserReleaseArticle
from subscriptions.views.misc import get_form_code
from utils.forms import all_valid
from social_core.actions import do_auth
from social_core.utils import setting_name
from social_django.utils import psa
from subscriptions.views.misc import get_form_code
from terms.views import UpdateUserView as TermsUpdateUser

from .forms import (
    LoginForm,
    CustomRegistrationFormTermsOfService,
    RegistrationFormTermsOfServiceUniqueEmail,
    ProfileEditForm
)
from .forms import CustomerDataForm
from .models import UserProfile


nl2space_re = re.compile("[\n\r]+", re.U)

social_namespace = getattr(
    settings, setting_name('URL_NAMESPACE'), None) or 'social'


def get_next_url(request):
    next_url = request.GET.get(auth.REDIRECT_FIELD_NAME, '')
    if next_url and is_safe_url(url=next_url, host=request.get_host()):
        return next_url
    return None


def login(request):
    login_form = LoginForm(None, request.POST or None, prefix='login')

    if login_form.is_valid():

        # save site to logged_sites
        site = Site.objects.get_current()
        username = login_form.cleaned_data.get('username')
        try:
            user = User.objects.get(username__iexact=username)
        except:
            user = User.objects.get(email__iexact=username)
        user_profile = UserProfile.objects.get(user=user)
        user_profile.logged_sites.add(site)

        # continue logging in
        expiry = settings.SESSION_COOKIE_AGE \
            if login_form.cleaned_data.get('remember_me') else 0
        request.session.set_expiry(expiry)
        response = django_login(
            request, 'auth_ex/auth.html',
            redirect_field_name='next',
            authentication_form=partial(LoginForm, prefix='login'))
        response.delete_cookie('hide_msg')
        return response

    registration_form = CustomRegistrationFormTermsOfService()
    next_url = get_next_url(request) or reverse_lazy('auth:profile')
    tos_url = Site.objects.get_current().configuration.tos_url

    return render(request, 'auth_ex/auth.html', {
        'login_form': login_form,
        'registration_form': registration_form,
        'next_url': next_url,
        'tos_url': tos_url,
    })


@login_required
def profile_shopping_history(request):
    user_releases = UserReleaseArticle.objects.filter(
        user=request.user
    )
    user_releases_pdf = UserPDFRelease.objects.filter(
        user=request.user
    )
    articles_payments = Payment.objects.filter(
        order__in=request.user.order_set.all(),
        order__content_type__model='article',
    ).select_related(
        'order',
    )

    return render(request, 'auth_ex/profile_shopping_history.html', {
        'user_releases': user_releases,
        'user_releases_pdf': user_releases_pdf,
        'articles_payments': articles_payments,
    })


@login_required
def profile(request):
    site = Site.objects.get_current()
    configuration = site.configuration
    user = request.user
    subscriptions = request.user.magazine_subscriptions.select_related(
        'period',
    ).order_by(
        '-created_at',
    )
    user_releases = UserReleaseArticle.objects.filter(
        user=request.user, is_paid=True
    )
    user_releases_pdf = UserPDFRelease.objects.filter(
        user=request.user, is_paid=True
    )
    user_galleries = UserGallery.objects.filter(
        added_by=request.user).distinct()
    subscription = (user.magazine_subscriptions.active()[0:1] or
                    user.promotional_subscriptions.active()[0:1] or None)
    account_status = (
        _('Subscriber')
    ) if subscription else (
        _('Non-subscriber')
    )
    paid_articles_limit = configuration.paid_articles_limit
    user_read_article_count = max(0, paid_articles_limit - user.read_articles.count())
    releases = Release.objects.filter(site=site).order_by('-date')
    try:
        release = releases[0]
    except IndexError:
        release = None

    form_code, form = get_form_code(request)

    return render(request, 'auth_ex/profile.html', {
        'user': user,
        'subscription':subscription,
        'subscriptions': subscriptions,
        'user_releases': user_releases,
        'user_releases_pdf': user_releases_pdf,
        'user_galleries': user_galleries,
        'account_info_url': configuration.account_info_url,
        'account_status': account_status,
        'form': form,
        'form_code': form_code,
        'paid_articles_limit': paid_articles_limit,
        'user_read_article_count': user_read_article_count,
        'release': release,
    })


@login_required
def profile_edit(request):
    profile_form = ProfileEditForm(
        request.POST or None, instance=request.user.profile)
    good_category_formset = UserGoodCategoryFormSet(
        request.POST or None, instance=request.user)
    goods_formset = GoodsFormSet(request.POST or None, instance=request.user)

    next_url = get_next_url(request) or reverse('auth:profile')

    if request.method == 'POST':
        if all_valid(profile_form, good_category_formset, goods_formset):
            profile_form.save()
            good_category_formset.save()
            goods_formset.save()
            profile_form.send_email()
            TermsUpdateUser.as_view()(request, user_required=True)
            messages.success(request, _('Profile data saved successfully'))
            return HttpResponseRedirect(next_url)
        else:
            errors = profile_form.errors.as_data()
            for msg in errors:
                for error in profile_form.errors[msg]:
                    txt = '{} - {}'.format(profile_form.fields[msg].label, error)
                    messages.error(request, txt)

    tos_url = Site.objects.get_current().configuration.tos_url

    return render(request, 'auth_ex/profile_edit.html', {
        'profile_form': profile_form,
        'good_category_formset': good_category_formset,
        'goods_formset': goods_formset,
        'next_url': next_url,
        'tos_url': tos_url,
    })

@login_required
def profile_password_reset(request):
    user = request.user
    return render(request, 'auth_ex/profile_password_reset.html', {
        'form': PasswordResetForm,
        'user': user,
    })



def password_reset(request, *args, **kwargs):
    if request.GET.get('back', None) is not None:
        back_url = request.GET.get('back')
    else:
        back_url = reverse_lazy('auth:login')
    email = request.POST.get('email')
    user_exists = User.objects.filter(email__iexact=email, is_active=True). \
        exists()
    if request.method == "POST" and not user_exists:
        response = render(request, 'registration/password_reset_failed.html')
    else:
        response = django_password_reset(request, extra_context={
            'back_url': back_url}, *args, **kwargs)
    return response


@login_required
def profile_release_pdfs(request):
    pdf_releases = UserPDFRelease.objects.filter(user=request.user)\
                                         .order_by('-created')

    return render('auth_ex/profile_release_pdfs.html', {
        'user': request.user,
        'pdf_releases': pdf_releases,
    })


@login_required
def profile_release_articles(request):
    article_releases = UserReleaseArticle.objects.filter(user=request.user)\
                                                 .order_by('-created')

    return render('auth_ex/profile_release_articles.html', {
        'user': request.user,
        'article_releases': article_releases,
    })


def _api_decorators(fx): return ensure_csrf_cookie(
    fx if settings.DEBUG else require_POST(fx)
)


@_api_decorators
def api_create_user(request):
    FIELDS = ('email', 'password', 'first_name', 'last_name', 'url')
    TEXT = 'text/plain; charset=utf-8'

    login_form = LoginForm(None, request.POST or None, prefix='login')
    if request.method == 'POST':
        user = login_form.get_user() if login_form.is_valid() else request.user

        if not (user.is_authenticated() and (
            user.is_superuser or user.has_perm('auth.add_user')
        )):
            return HttpResponseForbidden('Wrong credentials')

        try:
            email, password, first_name, last_name, url = (
                request.POST.get(key) for key in FIELDS
            )
            if not (email and password) or len(email) > 30 or email.find('@') < 1:
                raise ValueError

            if User.objects.filter(email=email).exists():
                return HttpResponse(
                    'Email exists', status=406, content_type=TEXT)

            user = User.objects.create_user(email, email)
            user.password = password
            user.save()

            if first_name or last_name or url:
                profile, _created = UserProfile.objects.get_or_create(
                    user=user)
                profile.first_name = first_name
                profile.last_name = last_name
                profile.registration_source = url
                profile.save()
        except (KeyError, ValueError, DatabaseError) as ex:
            err = 'Integrity error' if isinstance(ex, DatabaseError) else (
                'Missing data' if isinstance(ex, KeyError) else 'Wrong data'
            )
            err = "%s\n%s" % (err, ex)
            return HttpResponse(err, status=406, content_type=TEXT)
        else:
            return HttpResponse('OK', content_type=TEXT)
    elif settings.DEBUG:
        return HttpResponse(
            """<meta charse='utf-8'><form method=post>
                %s <hr> %s <input name=csrfmiddlewaretoken id=csrf>
                <button type=submit>OK</button>
                <script>
                    var ca = document.cookie.split(';');
                    for(var i=0; i<ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0)==' ') c = c.substring(1);
                        if (c.indexOf("csrftoken=") == 0)
                            document.getElementById('csrf').value = c.substring(10,c.length);
                    }
                </script>
            </form>""" % (login_form.as_p(), "\n".join(
                map("<p>{0}: <input name='{0}'></p>".format, FIELDS)
            ))
        )


class ActivationView(OriginalActivationView):
    def get_success_url(self, user):
        next_url = get_next_url(self.request)
        if next_url and next_url != '/accounts/profile/':
            messages.success(self.request, _('Account activated'))
            return next_url

        return reverse('registration:activation_complete')


class RegistrationMixin(object):
    def get_success_url(self, user=None):
        self.request.session['registration_google_code'] = True

        next_url = get_next_url(self.request) or None
        if next_url is not None:
            site = Site.objects.get_current()
            msg = _(
                "Your profile has been successfully created. You "
                "should receive email with activation link shortly. After "
                "activating your account please log in below to continue."
            ) if site.configuration.registration_requires_activation else (_(
                "Your profile has been successfully created."
            ) if next_url != '/subscriptions/promotion/new/' else _(
                "Your profile has been successfully created, "
                "please select promotion you want to use."
            ))
            messages.success(self.request, msg)

        return next_url

    @atomic
    def register(self, form):
        site = Site.objects.get_current()
        new_user = RegistrationProfile.objects.create_inactive_user(
            form, site, False)

        next_url = self.request.GET.get(auth.REDIRECT_FIELD_NAME, '')

        registration_profile = new_user.registrationprofile
        if site.configuration.registration_requires_activation:
            ctx = {
                'activation_key': registration_profile.activation_key,
                'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
                'site_schema': settings.SITE_SCHEMA,
                'site': site, 'request': self.request, 'get_params':
                    is_safe_url(url=next_url, host=self.request.get_host())
                    and ('?' + urlencode({'next': next_url})) or ''
            }
            subject = render_to_string(
                'registration/activation_email_subject.txt', ctx)
            message = render_to_string(
                'registration/activation_email.txt', ctx)

            new_user.email_user(nl2space_re.sub(" ", subject).strip(),
                                message, settings.DEFAULT_FROM_EMAIL)
        else:
            new_user.is_active = True
            new_user.save()
            registration_profile.activation_key = RegistrationProfile.ACTIVATED
            registration_profile.save()

            self.request.session.set_expiry(0)
            auth.login(self.request, auth.authenticate(
                username=new_user.username,
                password=form.cleaned_data['password1']
            ))

        signals.user_registered.send(
            sender=self.__class__, user=new_user, request=self.request)

        TermsUpdateUser.as_view()(self.request, user_required=False)

        return new_user


class RegistrationView(RegistrationMixin, BaseRegistrationView):
    template_name = 'auth_ex/auth.html'
    form_class = RegistrationFormTermsOfServiceUniqueEmail

    def get_success_url(self, user):
        return super(RegistrationView, self).get_success_url(user) \
            or reverse('registration:complete')

    def get_form_class(self, request=None):
        request = request or self.request
        self.next_url = get_next_url(request)
        return self.form_class

    def get_context_data(self, **kwargs):
        context = super(RegistrationView, self).get_context_data(**kwargs)
        login_form = LoginForm(prefix='login')
        next_url = self.next_url or reverse_lazy('auth:profile')
        tos_url = Site.objects.get_current().configuration.tos_url
        context.update({
            'login_form': login_form,
            'registration_form': context['form'],
            'next_url': next_url,
            'tos_url': tos_url,
        })
        return context

    def registration_allowed(self):
        return getattr(settings, 'REGISTRATION_OPEN', True)


@never_cache
@psa('{0}:complete'.format(social_namespace))
def custom_fb_auth(request, backend):
    redirect_name = 'homepage:index'
    if 'subscriptions' in request.META.get('HTTP_REFERER', []):
        redirect_name = request.META['HTTP_REFERER']
    return do_auth(request.backend, redirect_name=redirect_name)
