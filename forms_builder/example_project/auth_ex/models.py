from datetime import timedelta

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from forms_builder.forms.models import Form
from phonenumber_field.modelfields import PhoneNumberField
from sorl.thumbnail import ImageField

from tinymce.models import HTMLField
from utils import random_hash_generator
from utils.shortcuts import template_email

User._meta.get_field('username').validators.pop(0)
User._meta.get_field('username').help_text = ''


def _get_full_name(self):
    """
    Returns the first_name plus the last_name, with a space in between.
    """
    profile = self.profile or self
    full_name = '%s %s' % (profile.first_name, profile.last_name)
    return full_name.strip() or self.get_username()


def _get_short_name(self):
    "Returns the short name for the user."
    return self.first_name or self.get_username()


def _get_username(self):
    "Return the identifying username for this User"
    nick = getattr(self, self.USERNAME_FIELD)
    if not nick:
        return None
    return nick[:nick.find('@')] if nick == self.email else nick


def _natural_key(self):
    return getattr(self, self.USERNAME_FIELD)


User.get_full_name = _get_full_name
User.get_short_name = _get_short_name
User.get_username = _get_username
User.natural_key = _natural_key


def _default_registration_source():
    try:
        site = Site.objects.get_current()
        return site.domain + "#other"
    except:
        return "#other"


class UserProfile(models.Model):
    INDIVIDUAL = 'individual'
    COMPANY = 'company'
    TYPE_CHOICES = (
        (INDIVIDUAL, _('Individual')),
        (COMPANY, _('Company')),
    )
    user = models.OneToOneField(
        User,
        related_name='profile',
    )
    type = models.CharField(
        _('User type'),
        max_length=32,
        default=INDIVIDUAL,
        choices=TYPE_CHOICES,
    )
    first_name = models.CharField(
        _('First name'),
        max_length=255,
        default='',
        blank=True,
    )
    last_name = models.CharField(
        _('Last name'),
        max_length=255,
        default='',
        blank=True,
    )
    description = HTMLField(
        _('Description'),
        null=True,
        blank=True,
    )
    form = models.ForeignKey(
        Form,
        related_name='users',
        null=True,
        blank=True,
    )
    photo = ImageField(
        _('Image'),
        upload_to='users_photos',
        null=True,
        blank=True,
    )
    is_expert = models.BooleanField(
        _('Expert'),
        default=False,
        blank=True,
        help_text=_("Check if you want the user to be an expert in expert's "
                    "questions."),
    )
    expert_description = HTMLField(
        _('Expert description'),
        blank=True,
        help_text=_("User description for expert's questions."),
    )
    business = models.CharField(
        _('Company name'),
        max_length=255,
        blank=True,
    )
    nip = models.CharField(
        _('NIP'),
        max_length=255,
        blank=True,
    )
    city = models.CharField(
        _('City'),
        max_length=255,
        default='',
        blank=True,
    )
    street = models.CharField(
        _('Street'),
        max_length=255,
        default='',
        blank=True,
    )
    local_number = models.CharField(
        _('Local number'),
        max_length=255,
        default='',
        blank=True,
    )
    zip_code = models.CharField(
        _('Zip code'),
        max_length=6,
        default='',
        blank=True,
    )
    birthday = models.DateField(
        _('Birthday'),
        null=True,
        blank=True,
    )
    phone = PhoneNumberField(
        _('Phone'),
        blank=True,
    )

    # correspondence data
    different_c_address = models.BooleanField(
        _('I want to change correspondence address'),
        default=False,
        blank=True,
    )
    c_city = models.CharField(
        _('City'),
        max_length=255,
        blank=True,
    )
    c_street = models.CharField(
        _('Street'),
        max_length=255,
        blank=True,
    )
    c_local_number = models.CharField(
        _('Local number'),
        max_length=255,
        blank=True,
    )
    c_zip_code = models.CharField(
        _('Zip code'),
        max_length=6,
        blank=True,
    )
    c_phone = PhoneNumberField(
        _('Phone'),
        blank=True,
    )

    customer_id = models.CharField(
        _('Customer ID'),
        max_length=64,
        blank=True,
    )
    card_id = models.CharField(
        _('Card ID'),
        max_length=64,
        blank=True,
    )

    registration_source = models.CharField(
        _('Registration source'),
        max_length=255,
        blank=True,
        default=_default_registration_source,
    )

    random_hash = models.CharField(
        _('Random secret hash'),
        default=random_hash_generator,
        max_length=5,
        unique=True,
        blank=False,
        null=False,
        editable=False,
    )

    privacy_policy = models.BooleanField(
        _('Privacy policy agreement'),
        help_text=_('I have read and accept terms and conditions.'),
        default=True,
    )
    data_processing = models.BooleanField(
        _('Consent to data processing'),
        default=False,
        help_text=_(
            'I consent to the processing of data for purposes of promotion and'
            ' marketing, information systems and other data collections with '
            'complete confidentiality. This includes the processing of data in'
            ' the future by the PWR or other entity resulting from the '
            'conversion PWR if the purpose of the processing will not change.'),
    )

    fav_markets = models.CommaSeparatedIntegerField(
        _('Markets app favourite companies ids'),
        default='',
        blank=True,
        max_length=65535,
    )

    logged_sites = models.ManyToManyField(
        Site,
        blank=True,
    )

    activation_code_attempts = models.PositiveSmallIntegerField(
        default=0,
        blank=True,
        null=False,
    )
    activation_code_ban = models.DateTimeField(
        null=True,
        blank=True,
    )
    all_sites_access = models.BooleanField(
        _('Access to all sites'),
        default=False,
    )

    class Meta:
        verbose_name = _('User profile')
        verbose_name_plural = _('Users profiles')

    def __str__(self):
        return _("%s's profile") % self.user.username

    def get_postal_address(self):
        name = self.business if self.type == self.COMPANY else (
            self.first_name + " " + self.last_name
        )
        address = "\n%s %s\n%s %s\n\ntel. %s" % ((
            self.street, self.local_number,
            self.zip_code, self.city,
            self.phone
        ) if not self.different_c_address else (
            self.c_street, self.c_local_number,
            self.c_zip_code, self.c_city,
            self.c_phone
        ))
        return name + address

    def is_empty(self):
        return self.first_name == '' or not self.phone

    def is_subscriber(self):
        user = self.user
        site = Site.objects.get_current()
        return user.is_authenticated() and (
            user.is_superuser or user.magazine_subscriptions.active().filter(
                type__site=site
            ).exists() or user.promotional_subscriptions.active().filter(
                site=site
            ).exists()
        )

    def is_banned_from_code_activation(self):
        if not self.activation_code_ban:
            return False
        cfg = Site.objects.get_current().configuration
        if not cfg.subscriptions_codes_tries:
            return False

        ban_time = timedelta(hours=cfg.subscriptions_codes_ban_time)
        ban_time = self.activation_code_ban + ban_time >= now()

        if self.activation_code_attempts >= cfg.subscriptions_codes_tries:
            return ban_time
        else:
            return ban_time and None

    def save(self, *args, **kwargs):
        if self.activation_code_attempts is None:
            self.activation_code_attempts = 0
        return super(UserProfile, self).save(*args, **kwargs)


class UserSite(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='sites')
    site = models.ForeignKey('sites.Site', related_name='users')

    class Meta:
        verbose_name = _('User site')
        verbose_name_plural = _('User sites')
        unique_together = ('user', 'site')


class PasswordChange(models.Model):
    added = models.DateTimeField(auto_now_add=True)
    email = models.EmailField(_('email address'),
                              null=False, blank=False, unique=True)
    saved = models.BooleanField(null=False, default=False)

    def purge(self, emails=None):
        ttl = now() - timedelta(1)
        self.objects.filter(saved=False, added__lte=ttl).delete()


@receiver(pre_save, sender=User)
def update_password(sender, instance, **kwargs):
    if not (instance.pk and  # NOQA
        User.objects.get(pk=instance.pk).password == instance.password
    ):
        manager = PasswordChange.objects
        q = {"email": instance.email}
        manager.filter(**q).exists() or manager.create(**q)


def profile_created_receiver(sender, instance, created, **kwargs):
    if created:
        site = Site.objects.get_current()
        template_email(
            site.configuration.registration_email_recipient,
            _('New user (%(email)s) registered on %(site)s') % {
                'email': instance.user.email,
                'site': site.name,
            },
            'registration/registration_profile_created',
            kwargs={
                'profile': instance,
                'user': instance.user,
                'site': site,
            }
        )
