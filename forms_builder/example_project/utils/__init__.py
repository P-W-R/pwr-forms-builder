import random


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        return x_forwarded_for.split(',')[0]
    else:
        return request.META.get('REMOTE_ADDR')


def random_hash_generator(length=5):
    """
    Generates random hash of characters [A-Za-z0-9]. The default length (5)
    gives approximately a billion combinations (exactly 916,132,832).
    """
    global alphanumeric
    return ''.join((random.choice(alphanumeric) for i in range(length)))
