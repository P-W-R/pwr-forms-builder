from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import Http404
from django.template.loader import render_to_string, TemplateDoesNotExist


def get_page_or_404(request, object_list, per_page, page_number=None,
                    default_page=1):
    try:
        if page_number is None:
            page_number = request.GET.get('page', default_page)
        return Paginator(object_list, per_page).page(page_number)
    except (EmptyPage, PageNotAnInteger):
        raise Http404


def template_email(emails, subject, template, truncate_whitespace=True,
                   sender=settings.DEFAULT_FROM_EMAIL, kwargs={},
                   attachments=None):
    message_txt = render_to_string('%s.txt' % template, kwargs).strip()

    if truncate_whitespace:
        message_txt = '\n'.join(
            line.strip() for line in message_txt.splitlines())

    if isinstance(emails, str):
        emails = [emails]

    mail = EmailMultiAlternatives(subject, message_txt, sender, emails)

    try:
        message_html = render_to_string('%s.html' % template, kwargs)
        mail.attach_alternative(message_html, 'text/html')
    except TemplateDoesNotExist:
        pass

    if attachments:
        if isinstance(attachments, str):
            mail.attach_file(attachments)
        else:
            for attachment in attachments:
                if isinstance(attachments, str):
                    mail.attach_file(attachment)
                elif len(attachment) > 2:
                    mail.attach(*attachment)
                else:
                    mail.attach_file(*attachment)

    try:
        mail.send()
        return True
    except:
        return False
